%% GPU variables
hv1080=gpuDevice(1);
gpu_trainmat=gpuArray(trainmat);
gpu_tournament=gpuArray(tournament);
gpu_weights=gpuArray(zeros(1,features));
gpu_compute_train=gpuArray(zeros(size(trainmat)));
gpu_compute=gpuArray(zeros(size(tournament)));
cpu_times=[];
gpu_times=[];

%% setup environment
% profile on
consistency=0;
logloss_tournament=10;
consistency_train=0;
% history_logloss=zeros(300000,1);
% history_consistency=zeros(300000,1);
% history_logloss_train=zeros(300000,1);
% history_consistency_train=zeros(300000,1);
% figure1=figure;
iter=0;
logloss_opt=10;
% terminate=0;
logloss_goal=0.6925;
consistency_goal=0.75;
beep on;

%% Self perfecting
tic

while consistency_train<consistency_goal || consistency<consistency_goal % logloss_tournament>logloss_goal   terminate==1 || 

    try
    weights=truerand(1,features); %here is the model
    catch me
    weights=rand(1,features); %here is the model 
    end
    gpu_weights=weights;
    
    gpu_weights=bsxfun(@rdivide, gpu_weights, sum(gpu_weights,2)); %not needed with a real model

    gpu_compute_train = bsxfun(@times, gpu_trainmat, gpu_weights);

    gpu_compute=bsxfun(@times, gpu_tournament, gpu_weights); %computing the weighted values

    solution_train=sum(gather(gpu_compute_train),2);
    solution=sum(gather(gpu_compute),2); %computing solution

    logloss_train=logLoss(target,solution_train);  
    
    parfor i=1:num_era
        sample=find(era==i)
        logloss_era(i)=logLoss(target(sample,:),solution_train(sample,:));
    end
    clearvars sample
    
    idx = logloss_era<-log(0.5);
    consistency_train=sum(idx)/num_era;

%     history_logloss_train(iter+1)=logloss_train;
%     history_consistency_train(iter+1)=consistency_train;
    
    logloss_tournament=logLoss(validtarget,solution(1:validationsize,:));

    parfor i=1:num_era_valid
        sample=find(era_valid==i+max(era));
        logloss_era_valid(i)=logLoss(validtarget(sample,:),solution(sample,:));
    end
    clearvars sample
    
    idx = logloss_era_valid<-log(0.5);
    consistency=sum(idx)/num_era_valid;
    
%     history_logloss(iter+1)=logloss_tournament;
%     history_consistency(iter+1)=consistency;

    if logloss_tournament<=logloss_opt
       logloss_opt=logloss_tournament;
       consistency_opt=consistency;
       weights_opt=gather(gpu_weights);
       solution_opt=gather(solution);
       if consistency>=0.75
           solution_upload=table(t_id, solution_opt, 'VariableNames', {'id' 'probability'});
           solution_weights=table(weights_opt);
           writetable(solution_upload,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_solutionRG_', int2str(logloss_opt*1000000),'.csv']);
           writetable(solution_weights,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_weightsRG.csv']);
           beep;
       end
    else
    end

    iter=iter+1;
    message=['Best logloss = ', num2str(logloss_opt,7),' with consistency = ', num2str(consistency_opt,2), ', iteration: ', num2str(iter), ', cuurent logloss = ', num2str(logloss_tournament,7),' with consistency = ', num2str(consistency,2)];
    message
    %axes1 = axes('Parent',figure1);
    %hold(axes1,'on');
    %scatter(history_logloss,history_consistency,'Marker','.');
    %scatter(history_logloss_train, history_consistency_train, [],'green','Marker','.');
    %xlim(axes1,[0.69 0.7]);
    %ylim(axes1,[0 1]);
    %annotation('textbox',[.15 .6 .3 .3],'String',{message},'FontSize',12,'FitBoxToText','on', 'BackgroundColor','white');
    %line([0.69 0.7], [consistency_goal consistency_goal], 'Color','red','LineStyle','--'); 
    %line([logloss_opt logloss_opt], [0 1], 'Color','yellow','LineWidth',2,'LineStyle',':'); 
    %linkdata on;
    %drawnow;    

end

toc
%% Export
%solution_upload=table(t_id, solution_opt, 'VariableNames', {'id' 'probability'});
%solution_weights=table(weights_opt);
%writetable(solution_upload,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_solutionRG.csv']);
%writetable(solution_weights,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_weightsRG.csv']);
%clearvars solution_upload solution_weights i;

% idx= history_logloss==0;
% history_logloss(idx)=[];
% history_consistency(idx)=[];
% history_logloss_train(idx)=[];
% history_consistency_train(idx)=[];

%if gpu_times==0
%    gpu_times=toc;
%else
%    gpu_times=[gpu_times, toc];
%end
    