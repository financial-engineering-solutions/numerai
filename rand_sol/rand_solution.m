%% setup environment
% profile on
consistency=0;
logloss_tournament=10;
%history_logloss=zeros(30000,1);
%history_consistency=zeros(30000,1);
%history_logloss_train=zeros(30000,1);
%history_consistency_train=zeros(30000,1);
%figure1=figure;
iter=0;
logloss_opt=10;
consistency_opt=0;
%terminate=0;
logloss_goal=0.6925;
consistency_goal=0.75;
%rand_base=0.46;
%rand_limit=(0.5-rand_base)*2;
beep on;

%% Self perfecting
tic
%for s=1:50
    s=5;
    rand_base=0.5-s/100;
    rand_limit=(0.5-rand_base)*2;


    while consistency<consistency_goal || logloss_tournament>logloss_goal   %terminate==1 || 

        solution=rand_base+rand_limit*rand(size(t_id,1), 1); %computing solution

        logloss_tournament=logLoss(validtarget,solution(1:validationsize,:));

        for i=1:num_era_valid
            sample=find(era_valid==i+max(era));
            logloss_era_valid(i)=logLoss(validtarget(sample,:),solution(sample,:));
        end
        clearvars sample

        idx = logloss_era_valid<-log(0.5);
        consistency=sum(idx)/num_era_valid;

        %history_logloss(iter+1)=logloss_tournament;
        %history_consistency(iter+1)=consistency;

        if logloss_tournament<=logloss_opt && consistency>=consistency_goal
           logloss_opt=logloss_tournament;
           consistency_opt=consistency;
           solution_opt=gather(solution);
           solution_upload=table(t_id, solution_opt, 'VariableNames', {'id' 'probability'});
           writetable(solution_upload,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_solutionRS_', int2str(logloss_opt*1000000),'.csv']);
           beep;
        else
        end

        iter=iter+1;
        message=['Best logloss = ', num2str(logloss_opt,7),' with consistency = ', num2str(consistency_opt,2), ', iteration: ', num2str(50-s), num2str(-iter), ', cuurent logloss = ', num2str(logloss_tournament,7),' with consistency = ', num2str(consistency,2)];
        message
        %axes1 = axes('Parent',figure1);
        %hold(axes1,'on');
        %scatter(history_logloss,history_consistency,'Marker','.');
        %scatter(history_logloss_train, history_consistency_train, [],'green','Marker','.');
        %xlim(axes1,[0.69 0.7]);
        %ylim(axes1,[0 1]);
        %annotation('textbox',[.15 .6 .3 .3],'String',{message},'FontSize',12,'FitBoxToText','on', 'BackgroundColor','white');
        %line([0.69 0.7], [consistency_goal consistency_goal], 'Color','red','LineStyle','--'); 
        %line([logloss_opt logloss_opt], [0 1], 'Color','yellow','LineWidth',2,'LineStyle',':'); 
        %linkdata on;
        %drawnow;    

    end
    logloss_tournament=10;
    consistency=0;
beep
beep
beep
beep

%end
toc
%% Export
%solution_upload=table(t_id, solution_opt, 'VariableNames', {'id' 'probability'});
%solution_weights=table(weights_opt);
%writetable(solution_upload,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_solutionRG.csv']);
%writetable(solution_weights,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_weightsRG.csv']);
%clearvars solution_upload solution_weights i;

% idx= history_logloss==0;
% history_logloss(idx)=[];
% history_consistency(idx)=[];
% history_logloss_train(idx)=[];
% history_consistency_train(idx)=[];

%if gpu_times==0
%    gpu_times=toc;
%else
%    gpu_times=[gpu_times, toc];
%end
    