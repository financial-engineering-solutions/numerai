```matlab

%% setup environment

%% Self perfecting
tic
weights=ones(1,features)/features; %here is the model 
compute=bsxfun(@times, tournament, weights); %computing the weighted values
solution=sum(compute,2); %computing solution
logloss_tournament=logLoss(validtarget,solution(1:validationsize,:));

for i=1:num_era_valid
    sample=find(era_valid==i);
    logloss_era_valid(i)=logLoss(validtarget(sample,:),solution(sample,:));
end
clearvars sample

idx = logloss_era_valid<-log(0.5);
consistency=sum(idx)/num_era_valid;
solution_opt=solution;
weights_opt=weights;
toc

%% Export
solution_upload=table(t_id, solution_opt, 'VariableNames', {'id' 'probability'});
solution_weights=table(weights_opt);
writetable(solution_upload,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_solutionS.csv']);
writetable(solution_weights,[datestr(now, 'yyyy-mm-dd-HH-MM'),'_weightsS.csv']);
clearvars solution_upload solution_weights i;

```